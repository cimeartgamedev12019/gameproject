unit GameModule;
    
interface
    
uses SysUtils, Crt, Math, Engine,
     CSFMLConfig, CSFMLAudio, CSFMLGraphics, CSFMLNetwork, CSFMLSystem, CSFMLWindow;
    
{$include MyGame\MyGame.decl.inc}

implementation
    
{$include MyGame\MyGame.impl.inc}

end.