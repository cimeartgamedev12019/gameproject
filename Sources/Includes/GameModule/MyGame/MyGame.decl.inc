type
    MyGame = class(Game)
    public
        GameWindow : UIWindow;

    public
        procedure Run; override;
        procedure Update; override;
        procedure Render; override;
        procedure Shutdown; override;
        constructor Create; override;
        
    end;