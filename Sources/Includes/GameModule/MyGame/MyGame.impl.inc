procedure MyGame.Run;
begin
    GameWindow := UIWindow.Create(@self, 1280, 720);
    inherited Run;
end;

procedure MyGame.Update();
begin
    GameWindow.Update;
    inherited Update;
end;

procedure MyGame.Render();
begin
    sfRenderWindow_Clear(GameWindow.GetHandle(), sfBlue);
    inherited Render;
end;

procedure MyGame.Shutdown();
begin
    inherited Shutdown;
end;

constructor MyGame.Create();
begin
    
end;