unit Engine;

interface

uses Classes, Crt, TypInfo, Windows, SysUtils, Math, ZStream, FGL,
     DOM, XMLRead, XMLWrite, XMLConf, XMLUtils, XMLStreaming,
     CSFMLConfig, CSFMLAudio, CSFMLGraphics, CSFMLNetwork, CSFMLSystem, CSFMLWindow;

type
  TFileStreamHelper = class helper for TFileStream
    procedure WriteString(const Str : String);
    procedure WriteBytes(Bytes : Array of Byte);
    function SearchBytes(StartPosition : Int64; BytesPattern : Array of Byte) : Int64;
  end;

{$include Game\Game.decl.inc}
{$include Archive\GameArchive.decl.inc}
{$include Log\Log.decl.inc}
{$include Dialog\Dialog.decl.inc}
{$include Graphics\UI\Window\Window.decl.inc}

{$include Game\GameObject.decl.inc}
{$include SceneSystem\Scene.decl.inc}
{$include SceneSystem\SceneManager.decl.inc}

function BinToAscii(const bin: array of byte): AnsiString;
function BinToStr(const bin: array of byte): string;

function ExtractFileNameEX(const AFileName:String): String;
function GetApplicationPath() : String;
function GetConsoleWindow: HWND; stdcall; external kernel32;
procedure SetTerminalTitle(Title : String);
procedure InitEngine;

implementation

procedure TFileStreamHelper.WriteString(const Str : String);
begin
    self.write(Str[1],Length(Str));
end;

procedure TFileStreamHelper.WriteBytes(Bytes : Array of Byte);
var AByte : Byte;
begin
    for AByte in Bytes do
    begin
        self.WriteByte(AByte);
    end;
end;

function TFileStreamHelper.SearchBytes(StartPosition : Int64; BytesPattern : Array of Byte) : Int64;
var
    AByte : Byte;
    Idx : Integer;
begin
    Result := -1;
    self.Position := StartPosition;
    while (self.Position <= self.Size - 1) do
    begin
        AByte := self.ReadByte;

        if (AByte = BytesPattern[Low(BytesPattern)]) then
        begin
            if (Length(BytesPattern) = 1) then
            begin
                Result := self.Position - 1;
                self.Position := StartPosition;
                Exit(Result);
            end;

            for Idx := Low(BytesPattern) + 1 to High(BytesPattern) do
            begin
                AByte := self.ReadByte;
                if BytesPattern[Idx] <> AByte then break;
            end;

            if Idx <> High(BytesPattern) then
            begin
                Result := -2;
                self.Position := StartPosition;
                Exit(Result); //First byte and maybe more was found but not all bytes so Result = -2
            end else begin
                Result := self.Position - Length(BytesPattern);
                self.Position := StartPosition;
                Exit(Result); //All bytes were founds we return the position of the pattern beginning in FileStream
            end;
        end;
    end;
    Log.Write('BytesPattern not found', ELogType.Warning, 'Memory');
    Exit(Result); //Result will always be equal to -1 here
end;

{$include Game\Game.impl.inc}
{$include Archive\GameArchive.impl.inc}
{$include Log\Log.impl.inc}
{$include Dialog\Dialog.impl.inc}
{$include Graphics\UI\Window\Window.impl.inc}

{$include Game\GameObject.impl.inc}
{$include SceneSystem\Scene.impl.inc}
{$include SceneSystem\SceneManager.impl.inc}

function BinToAscii(const bin: array of byte): AnsiString;
var i: integer;
begin
    SetLength(Result, Length(bin));
    for i := 0 to Length(bin)-1 do
      Result[1+i] := AnsiChar(bin[i]);
end;

function BinToStr(const bin: array of byte): string;
const HexSymbols = '0123456789ABCDEF';
var i: integer;
begin
    SetLength(Result, 2*Length(bin));
    for i :=  0 to Length(bin)-1 do begin
        Result[1 + 2*i + 0] := HexSymbols[1 + bin[i] shr 4];
        Result[1 + 2*i + 1] := HexSymbols[1 + bin[i] and $0F];
    end;
end;

function ExtractFileNameEX(const AFileName:String): String;
var I: integer;
begin
    I := LastDelimiter('.'+PathDelim+DriveDelim,AFileName);
    if (I=0) or (AFileName[I] <> '.') then
        I := MaxInt;
    Result := ExtractFileName(Copy(AFileName,1,I-1));
end;

function GetApplicationPath() : String;
begin
    Result := ExtractFileDir(ParamStr(0));
end;

procedure SetTerminalTitle(Title : String);
begin
    SetConsoleTitle(PChar(Title));
end;

procedure InitEngine;
var ParamIndex : Integer;
begin
    ShowWindow(GetConsoleWindow, SW_HIDE);
    SetTerminalTitle('GAME ENGINE - DEBUG CONSOLE');
    
    for ParamIndex := 1 to ParamCount do
    begin
        if ParamStr(ParamIndex) = '-debug' then
        begin
            ShowWindow(GetConsoleWindow, SW_SHOW);
        end;
    end;
end;

end.