type
    ELogType = (Success = Green, Info = LightBlue, Error = LightRed, Warning = Yellow, Normal = White);
    Log = class
        class procedure Write(Message : String; Severity : ELogType = ELogType.Normal; Context : String = 'ENGINE'); static;
    end;