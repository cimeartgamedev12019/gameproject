class procedure Log.Write(Message : String; Severity : ELogType = ELogType.Normal; Context : String = 'ENGINE');
begin
    TextColor(Ord(Severity));
    WriteLn('[', UpperCase(Context), ' - ', TimeToStr(Time), '] ', Message);
    TextColor(Ord(ELogType.Normal));
end;