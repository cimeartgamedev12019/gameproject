(*
    ClassName : UIWindow
    Usage : This class is made to provide a rendering surface to work on.
            It's used by the game engine to display the result of all the rendering commands
    Unit : Engine
    Location :  Graphics\UI\Window
*)

constructor UIWindow.Create(GamePtr : Pointer; Width, Height : Integer; Title : String = 'Untitled window');
begin
    Log.Write('Intializing window...', ELogType.Normal, 'UIWindow');
    self.VideoMode.Width := Width;
    self.VideoMode.Height := Height;
    self.VideoMode.BitsPerPixel := 32;
    self.GameReference := GamePtr;
    self.WindowHandle := sfRenderWindow_Create(self.VideoMode, PChar(Title), sfUint32(sfResize) or sfUint32(sfClose), nil);
end;

function UIWindow.GetHandle() : PsfRenderWindow;
begin
    GetHandle := self.WindowHandle;
end;

procedure UIWindow.SetTitle(Title : String);
begin
    sfRenderWindow_setTitle(self.WindowHandle, PChar(Title));
end;

procedure UIWindow.Show();
begin
    sfRenderWindow_setActive(self.WindowHandle, sfTrue);
    sfRenderWindow_setVisible(self.WindowHandle, sfTrue);
end;

procedure UIWindow.Hide();
begin
    sfRenderWindow_setVisible(self.WindowHandle, sfFalse);
end;

procedure UIWindow.Update();
begin
    if (sfRenderWindow_IsOpen(self.WindowHandle) = sfTrue) then
    begin
        if (sfRenderWindow_PollEvent(self.WindowHandle, @EventsBuffer) = sfTrue) then
        begin
            if (EventsBuffer.type_ = sfEvtClosed) then begin
                self.OnClosed();
            end;

            if (EventsBuffer.type_ = sfEvtResized) then begin
                self.OnResized(EventsBuffer.size.width, EventsBuffer.size.height);
            end;

            if (EventsBuffer.type_ = sfEvtKeyPressed) then begin
                self.OnKeyPressed(EventsBuffer.key.code);
            end;

            if (EventsBuffer.type_ = sfEvtKeyReleased) then begin
                self.OnKeyReleased(EventsBuffer.key.code);
            end;
        end;

        sfRenderWindow_Clear(WindowHandle, sfBlack);

        GameReference^.Render();

        sfRenderWindow_display(WindowHandle);
    end;
end;

procedure UIWindow.OnClosed();
begin
    Log.Write('Closing window...', ELogType.Info, 'UIWindow');
    sfRenderWindow_Close(self.WindowHandle);
    GameReference^.Shutdown();
end;

procedure UIWindow.OnResized(Width, Height : Integer);
begin
    
end;

procedure UIWindow.OnKeyPressed(PressedKey : sfKeyCode);
begin
    
end;

procedure UIWindow.OnKeyReleased(ReleasedKey : sfKeyCode);
begin
    
end;