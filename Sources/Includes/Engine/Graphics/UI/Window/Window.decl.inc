type
    UIWindow = class
    private
        VideoMode : sfVideoMode;
        WindowHandle : PsfRenderWindow;
        WindowTitle : String;
        EventsBuffer : sfEvent;
        GameReference : ^Game;
    public
        constructor Create(GamePtr : Pointer; Width, Height : Integer; Title : String = 'Untitled window');
        procedure SetTitle(Title : String);
        procedure Show();
        procedure Hide();
        procedure Update();

    public
        function GetHandle() : PsfRenderWindow;

    public (* Window Events *)
        procedure OnClosed();
        procedure OnResized(Width, Height : Integer);
        procedure OnKeyPressed(pressedKey : sfKeyCode);
        procedure OnKeyReleased(releasedKey : sfKeyCode);

    public
        property Title: String read WindowTitle write SetTitle;
        
    end;