(*
    ClassName : GameArchive
    Description : Class used to store/manage game assets
    Unit : Engine
    Location : Engine/Archive
*)

type
    HeaderInfo = record
        BeginOffset : Int64;
        EndOffset : Int64;
    end;

    EAssetType = (Unknown = $B0, TextFile, BinaryFile, SceneLayout, GameScript, Canvas);

    FileInfo = class
        FileName : String;
        Offset : Int64;
        Size : Int64;
        AssetType : EAssetType;
        constructor Create(AFileName : String; AAssetType : EAssetType; AOffset, ASize : Int64);

    end;

    FileMap = specialize TFPGMap<String, FileInfo>;

    GameArchive = class
    private const
        ArchiveMagic : array[0..5] of byte = (71, 88, 97, 14, 07, 01); //GXa??? [ASCII]
        HeaderCode : array[0..5] of byte = (08, 05, 01, 04, 05, 18); //HEADER [A1Z26]
        NoFileCode : array[0..5] of byte = (14, 15, 04, 01, 20, 01); //NODATA [A1Z26]
        HeaderEnd : array[0..3] of byte = (08, 05, 14, 04); //HEND [A1Z26]
        FileEnd : array[0..1] of byte = ($ED, $FE); //237, 254 [DECIMAL]

    private
        ArchivePath : String;
        ArchiveName : String;
        ArchiveStream : TFileStream;
        
    public
        ArchiveHeaderInfo : HeaderInfo;
        ArchiveFileMap : FileMap;

    private
        procedure BuildHeaderInfo();
        procedure BuildFileMap();
        procedure New();
        procedure Open();

    public
        function ReadFile(FileName : String) : TFileStream;
        constructor Init(AArchivePath : String);

    end;