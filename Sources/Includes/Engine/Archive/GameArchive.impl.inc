{procedure GameAsset.Compress();
var DestinationPath : String;
begin
    CompressedFileStream := TMemoryStream.Create();
    Compressor := TCompressionStream.Create(clMax, CompressedFileStream);
    Compressor.CopyFrom(FileStream, FileStream.Size);
    Compressor.Free;
    

    WriteLn('ASSET COMPRESSED !');
end;

procedure GameAsset.Decompress();
begin
    FileStream.Free;
    FileStream := TMemoryStream.Create();

    CompressedFileStream.Position := 0;
    Decompressor := TDecompressionStream.Create(CompressedFileStream);


    WriteLn('ASSET DECOMPRESSED !');
end;

procedure GameAsset.SaveAsset();
begin
    CompressedFileStream.SaveToFile(ExtractFilePath(AssetPath) + '\' + ExtractFileNameEX(AssetPath) + '.xg' + ExtractFileExt(AssetPath));
end;

constructor GameAsset.Create(Path : String);
begin
    self.AssetPath := Path;
    FileStream := TMemoryStream.Create();
    FileStream.LoadFromFile(AssetPath);
    SetString(AssetContent, PChar(FileStream.Memory), FileStream.Size div SizeOf(Char));
end;}

{//Test.txt
ArchiveStream.WriteByte($B1);
ArchiveStream.WriteString('Demo/Test.txt');
ArchiveStream.WriteByte($A0); //FBegin File offset
ArchiveStream.WriteQWord(514); //Start at byte 514
ArchiveStream.WriteByte($A1); //Begin File Size
ArchiveStream.WriteQWord(32); //File size : 32 bytes
ArchiveStream.WriteBytes(FileEnd); //End of file

//DebugScene.level
ArchiveStream.WriteByte($B3);
ArchiveStream.WriteString('Data/DebugScene.level');
ArchiveStream.WriteByte($A0); //Begin File offset
ArchiveStream.WriteQWord(600); //Start at byte 600
ArchiveStream.WriteByte($A1); //Begin File Size
ArchiveStream.WriteQWord(314); //File size : 214 bytes
ArchiveStream.WriteBytes(FileEnd); //End of file}

constructor FileInfo.Create(AFileName : String; AAssetType : EAssetType; AOffset, ASize : Int64);
begin
    self.FileName := AFileName;
    self.AssetType := AAssetType;
    self.Offset := AOffset;
    self.Size := ASize;
end;

constructor GameArchive.Init(AArchivePath : String);
begin
    self.ArchivePath := AArchivePath;
    self.ArchiveName := ExtractFileName(AArchivePath);
    self.ArchiveFileMap := FileMap.Create();
    if FileExists(self.ArchivePath) then
    begin
        self.Open();
    end else begin
        self.New();
    end; 
end;

procedure GameArchive.BuildFileMap();
var
    FileNameBuffer : Array of Byte;
    CurrentByte, AssetByte : Byte;
    FileSize, FileOffset : QWord;
    EndFilePosition, EndFileName, EndOffsetPos : Int64;

begin
    ArchiveStream.Position := ArchiveHeaderInfo.BeginOffset;
    while (ArchiveStream.Position < ArchiveHeaderInfo.EndOffset) do
    begin
        CurrentByte := ArchiveStream.ReadByte;
        if (CurrentByte >= $B0) and (CurrentByte <= $BF) then
        begin
            AssetByte := CurrentByte;
            EndFilePosition := ArchiveStream.SearchBytes(ArchiveStream.Position, FileEnd);
            EndFileName := ArchiveStream.SearchBytes(ArchiveStream.Position, [$A0]);
            EndOffsetPos := ArchiveStream.SearchBytes(ArchiveStream.Position, [$A1]);

            SetLength(FileNameBuffer, EndFileName - ArchiveStream.Position); //Set FileNameBuffer length
            ArchiveStream.Read(FileNameBuffer[0], EndFileName - ArchiveStream.Position); //Read file name
            
            ArchiveStream.Position := EndFileName + 1;
            FileOffset := ArchiveStream.ReadQWord;

            ArchiveStream.Position := EndOffsetPos + 1;
            FileSize := ArchiveStream.ReadQWord;

            ArchiveFileMap[BinToAscii(FileNameBuffer)] := FileInfo.Create(BinToAscii(FileNameBuffer), EAssetType(AssetByte), FileOffset, FileSize);

            ArchiveStream.Position := EndFilePosition + 2;
        end;
    end;
end;

procedure GameArchive.BuildHeaderInfo();
var
    EndingBytePosition : Int64;

begin
    self.ArchiveHeaderInfo.BeginOffset := 27;
    ArchiveStream.Position := 27;
    EndingBytePosition := ArchiveStream.SearchBytes(ArchiveStream.Position, HeaderEnd);

    if EndingBytePosition >= 0 then
    begin
        self.ArchiveHeaderInfo.EndOffset := EndingBytePosition;
        Log.Write('Header ending bytes found at position ' + IntToStr(EndingBytePosition), ELogType.Success, 'GameArchive (' + self.ArchiveName + ')');
    end else begin
        Log.Write('Header ending bytes not found... Is GameArchive corrupted ?', ELogType.Error, 'GameArchive (' + self.ArchiveName + ')');
    end;
end;

procedure GameArchive.New();
begin
    Log.Write('Creating GameArchive ' + ExtractFileName(self.ArchivePath), ELogType.Normal, 'GameArchive (' + self.ArchiveName + ')');
    ArchiveStream := TFileStream.Create(self.ArchivePath, fmCreate);

    ArchiveStream.WriteBytes(ArchiveMagic);
    ArchiveStream.WriteString('VFS_GAMEARCHIVE');
    ArchiveStream.WriteBytes(HeaderCode);
    ArchiveStream.WriteBytes(NoFileCode);
    ArchiveStream.WriteBytes(HeaderEnd);

    self.BuildHeaderInfo();
    self.BuildFileMap();
end;

procedure GameArchive.Open();
begin
    Log.Write('Opening GameArchive ' + ExtractFileName(self.ArchivePath), ELogType.Normal, 'GameArchive (' + self.ArchiveName + ')');
    ArchiveStream := TFileStream.Create(self.ArchivePath, fmOpenReadWrite);

    self.BuildHeaderInfo();
    self.BuildFileMap();
end;

function GameArchive.ReadFile(FileName : String) : TFileStream; //TODO : Return GameAsset (This class contain a TFileStream member AssetData)
begin
    if (ArchiveStream.SearchBytes(ArchiveHeaderInfo.BeginOffset, NoFileCode) < 0) then
    begin
        Log.Write('Reading asset at ' + FileName, ELogType.Normal, 'GameArchive (' + self.ArchiveName + ')');
        WriteLn('ASSET FILE NAME : ', ArchiveFileMap[FileName].FileName);
        WriteLn('ASSET TYPE : ', ArchiveFileMap[FileName].AssetType);
        WriteLn('ASSET OFFSET : ', ArchiveFileMap[FileName].Offset);
        WriteLn('ASSET SIZE : ', ArchiveFileMap[FileName].Size);

        //READ FILE CONTENT TO GAMEASSET : GameAsset.AssetData.WriteBuffer(ByteArray[0], Length(ByteArray)); //Write the read data (ByteArrays) in the GameAsset.AssetData
    end else begin
        Log.Write('This GameArchive is empty... There''s no file to read in.', ELogType.Error, 'GameArchive (' + self.ArchiveName + ')');
    end;
end;