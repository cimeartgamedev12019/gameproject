type
    Game = class
    private
        isRunning : boolean;
        
    public
        procedure Run; virtual;
        procedure Update; virtual;
        procedure Render; virtual;
        procedure Shutdown; virtual;
        constructor Create; virtual;
        
    end;