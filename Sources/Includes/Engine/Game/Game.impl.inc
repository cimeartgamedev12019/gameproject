procedure Game.Run;
begin
    Log.Write('Game is running', ELogType.Success);
    self.isRunning := true;
    while self.isRunning do
    begin
        self.Update();
    end;

end;

procedure Game.Update();
begin
    
end;

procedure Game.Render();
begin

end;

procedure Game.Shutdown();
begin
    Log.Write('Shutting down game...', ELogType.Info);
    self.isRunning := False;
end;

constructor Game.Create();
begin
    
end;