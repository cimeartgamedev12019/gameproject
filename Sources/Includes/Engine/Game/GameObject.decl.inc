(*
    ClassName : GameObject
    Description : Abstract used containing all the game logic of a specialized object
    Unit : Engine
    Location : Engine/Game
*)

type
    GameObject = class
    private
        
    public
        procedure Update; virtual;
        procedure Render; virtual;
        constructor Create(); virtual;
    end;