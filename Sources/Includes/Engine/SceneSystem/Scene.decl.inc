(*
    ClassName : Scene
    Description : Scene class used to render and update the GameObjects of a level layout file
    Unit : Engine | SceneManager
    Location : Engine/SceneSystem
*)

type
    Scene = class
    public
        GameObjects : Array of GameObject;
        LevelLayout : TXmlDocument;
        
    public
        procedure Update();
        procedure Render();
        constructor Create(Layout : TXmlDocument);
    end;