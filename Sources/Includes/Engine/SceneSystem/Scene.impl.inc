procedure Scene.Update();
var aGameObject : GameObject;
begin
    for aGameObject in self.GameObjects do
    begin
        aGameObject.Update();
    end;
end;

procedure Scene.Render();
var aGameObject : GameObject;
begin
    for aGameObject in self.GameObjects do
    begin
        aGameObject.Render();
    end;
end;

constructor Scene.Create(Layout : TXmlDocument);
begin
    self.LevelLayout := Layout;
end;