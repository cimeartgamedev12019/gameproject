class procedure SceneManager.LoadScene(ScenePath : String; ShowLoadingScreen : Boolean);
var
    LevelDocument : TXmlDocument;
begin
    Log.Write('Loading scene at game://' + ScenePath, ELogType.Info);
    ReadXMLFile(LevelDocument, ScenePath);
    SceneManager.CurrentScene := Scene.Create(LevelDocument);
end;