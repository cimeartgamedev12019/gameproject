(*
    ClassName : SceneManager
    Description : Static class used to control the flow of the whole game scene management.
    Unit : Engine
    Location : Engine/SceneSystem
*)

type
    SceneManager = class
    public
        CurrentScene : Scene; static;
    public
        class procedure LoadScene(ScenePath : String; ShowLoadingScreen : Boolean); static;
    end;