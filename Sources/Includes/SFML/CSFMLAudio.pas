unit CSFMLAudio;

{$mode objfpc}{$H+}

interface

uses
  ctypes,
  CSFMLConfig,
  CSFMLSystem;

const
{$ifdef WINDOWS}
  LIB_CSFMLAUDIO = 'csfml-audio-2.dll';
{$endif}
{$ifdef LINUX}
  LIB_CSFMLAUDIO = 'libcsfml-audio.so';
{$endif}

{$include CSFMLAudioExport.inc}
{$include CSFMLAudioTypes.inc}
{$include CSFMLAudioSoundStatus.inc}

{$include CSFMLAudioListener.inc}
{$include CSFMLAudioMusic.inc}
{$include CSFMLAudioSound.inc}
{$include CSFMLAudioSoundBuffer.inc}
{$include CSFMLAudioSoundBufferRecorder.inc}
{$include CSFMLAudioSoundRecorder.inc}
{$include CSFMLAudioSoundStream.inc}

implementation

end.

