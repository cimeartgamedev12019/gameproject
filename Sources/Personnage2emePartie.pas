
Unit personnage2emepartie;
Interface 

USES crt;

{-------------------------weapon class---------------------}

type
	TWeapon = object
	StrWeapon,Description : string;
	AjoutStrength:integer;
	constructor Create(CStrWeapon,CDescription:string;CAjoutStrength:integer);
end;

{------------------------armor class-----------------------}

type
	TArmor = object
	strArmor ,description: string;
	Defence:integer;
	constructor create(CStrArmor,CDescription:string;CDefence:integer);
end;
	
{------------------------player class---------------------}
type
	TPersonnage = object
	Agility, Strength, Hp, Honnor, Defence, Level, XpTotal : integer;
	ItemSword:TWeapon;
	ItemArmor:TArmor;

	constructor Create(CAgility, CStrength, CHp, CHonnor, CDefence, CLevel, CXpTotal:integer);
end;

{------------------------ennemi class---------------------}

type
	TEnemy = object
	Hp, Attack, XpValue : integer;
	Nom:string;
	constructor Create(CHp, CAttack, CXpValue : integer;CNom:string);
	procedure AttackEsquive(CHp,CDamage:integer);
	procedure EsquiveEnemy;
end;

Implementation

{-----------Variables---------}

var 
	EsquiveBool:Boolean;

{--------------------Procedure----------------------}

procedure PrendChere(Hp,Damage:integer);
begin
        writeln('il vous met chere');
        hp := hp - damage;
        writeln('il vous inflige ',damage,' !!!');
end;

procedure MetChere(Hp,Damage:integer);
begin
    if EsquiveBool = false then
    begin
        writeln('vous lui mettez chere');
        Hp := Hp - Damage;
        writeln('vous lui infligez ',damage,' !!!');
    end;
end;
{-----------------------------------------------------constructor-------------------------------------------------}
{----------------------personnage parametre and procedure----------------------}
constructor TWeapon.Create(CStrWeapon,CDescription:string;CAjoutStrength:integer);
begin
	strWeapon:= CStrWeapon;
	AjoutStrength := CAjoutStrength;
	description := CDescription;
end;

constructor TArmor.Create(CStrArmor,CDescription:string;CDefence:integer);
begin
	StrArmor:= CStrArmor;
    Description:= CDescription;
    Defence := CDefence;
end;

constructor TPersonnage.Create(CAgility, CStrength, CHp, CHonnor, CDefence, CLevel, CXpTotal:integer);
begin
	Agility:= CAgility;
	Strength:= CStrength;
	Hp	:= CHp; 
	Honnor	:= CHonnor;
	Defence	:= CDefence;
	Level	:= CLevel;
	XpTotal	:= CXpTotal;
end;

{--------------------enemy parametre and procedure---------------------}

constructor TEnemy.Create(CHp, CAttack, CXpValue : integer;CNom:string);
begin
	Hp := CHp; 
	XpValue := CXpValue; 
	Attack := CAttack; 
	Nom := CNom;
end;


procedure TEnemy.EsquiveEnemy;
begin
	writeln('l''ennemi esquive l''attaque');
	EsquiveBool := true;
end;

procedure TEnemy.AttackEsquive(CHp,CDamage:integer);
var
	i,a:integer;
begin
	a := 3;
	i := Random(a)+1;
	if i = 1 then
	begin	
		EsquiveEnemy;
		a := a + 1;
	end
	else
	begin
		PrendChere(CHp,CDamage);
		a := 3;
	end;
end;

end.