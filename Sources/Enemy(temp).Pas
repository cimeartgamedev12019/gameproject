Unit Monster;

Interface

USES crt;

type
	Tenemy = object;
	level, hp, armor, shield, attack, vitesse_attack, penetration, xp_value : integer;
	critical_strike : real;
	name, type_enemy : string;
	showable, isDead, inFight, SkillUsed : boolean;
	coordinatesX, coordinatesY : integer;
	sprite:PsfSprite;
	CollisionDetection:=Trect
	procedure Movement();
	procedure WeaponAttack();
	procedure DamageOnContact();
	procedure SpawnXY();
	procedure SetFacing();
	procedure TargetPlayer();
	procedure ChasingTarget();
	procedure Dialog();
	procedure SpecialSkills();
	
end;

VAR
Goblin : Tenemy


	procedure Tenemy.Movement();
	begin
		{ sfSprite_create():PsfSprite; }
		{ sfSprite_setTexture(sprite: PsfSprite; const texture: PsfTexture; resetRect: sfBool); }
		{ sfSprite_getLocalBounds(const sprite: PsfSprite): sfFloatRect; }
		{ sfSprite_getGlobalBounds(const sprite: PsfSprite): sfFloatRect; }
		{ sfSprite_setOrigin(sprite: PsfSprite; origin: sfVector2f); }
		if TRect.Intersect(R:TRect)=0 then
		begin
			sfSprite_setPosition(sprite: PsfSprite; position: sfVector2f);
		end;
		
		if TRect.Intersect(R:TRect)<>0 then
		begin
			sfSprite_setScale(sprite: PsfSprite; scale: sfVector2f); 
		end;		
	end;
	
	procedure Tenemy.WeaponAttack();
	begin
	
	end;
	
	procedure Tenemy.DamageOnContact();
	begin
		if TRect.Intersect(R:TRect)<>0 then
		begin
			PrendChere(Hp,Damage:integer); 
		end;
	end;
	
	procedure Tenemy.SpawnXY();
	begin
		sfSprite_create():PsfSprite;
		sfSprite_setTexture(sprite: PsfSprite; const texture: PsfTexture; resetRect: sfBool);
		sfSprite_getLocalBounds(const sprite: PsfSprite): sfFloatRect;
		sfSprite_getGlobalBounds(const sprite: PsfSprite): sfFloatRect;
		sfSprite_setOrigin(sprite: PsfSprite; origin: sfVector2f);
	end;
	
	procedure Tenemy.SetFacing();
	begin
		if sfSprite_Player_getLocalBounds(const sprite: PsfSprite): sfFloatRect and sfSprite_Enemy_getLocalBounds(const sprite: PsfSprite): sfFloatRect < 10 then
		sfSprite_setScale(sprite: PsfSprite; scale: sfVector2f);
	end;
	
	procedure Tenemy.TargetPlayer();
	var
	distance:=single;
	begin
		distance:=sfSprite_getPosition(const spritePlayer: PsfSprite): sfVector2f - sfSprite_getPosition(const spriteEnemy: PsfSprite): sfVector2f;
		if distance <= une certaine mesure then//(que Marvin s'est porté volontaire pour faire !)
		setFacing();
		ChasingTarget();
		end;
	end;
	
	procedure Tenemy.ChasingTarget();
	begin
	
	end;
	procedure Tenemy.Dialog();
	begin
		if Tenemy.hp <=(Tenemy.hp*50/100) then
		showdialog(['...']);
		
		if Tenemy.hp<=0 then
		showdialog(['...']);
	end;
	procedure Tenemy.SpecialSkills();
	begin
		if inFight:=true then
		begin
			Attack
			if SkillUsed=true then
			begin
				delay(1000);
				SkillUsed:=false;
			end;
		end;		
	end;