program Main;

uses GameModule, Engine;

var
    GameInstance : MyGame;
    TestData : GameArchive;

begin
    InitEngine;
    
    TestData := GameArchive.Init('Data/TestComplex.gxa');
    TestData.ReadFile('UI/GameHUD.canvas');
    
    GameInstance := MyGame.Create;
    GameInstance.Run;
    
end.