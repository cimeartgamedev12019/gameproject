program Launcher;

{$APPTYPE CONSOLE}

uses SysUtils, Windows, Dialogs, Process;

var DebugOutput : String;

begin
    DebugOutput := '';
    if SetEnvironmentVariable('PATH', PChar(SysUtils.GetEnvironmentVariable('PATH') + ';' + ExtractFilePath(ParamStr(0)) + 'Libraries\')) then
    begin
        if ParamStr(1) = '-debug' then
        begin
            ExecuteProcess('.\Binaries\Main.exe', '-debug', []);
        end else begin
            ExecuteProcess('.\Binaries\Main.exe', '', []);
        end;
    end else begin
        MessageBox(0, 'Failed to run game program !' + sLineBreak + 'Make sure you have the required privileges to fully load the game data on your computer.' + sLineBreak + sLineBreak + 'Running the game as administrator could solve the issue...', 'Game initialization error', MB_ICONERROR);
    end;
end.