﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Collections.Generic;
using System.Text;

namespace GameArchive_Manager
{
    public struct HeaderInfo
    {
        public long BeginOffset;
        public long EndOffset;

        public HeaderInfo(long beginOffset, long endOffset)
        {
            BeginOffset = beginOffset;
            EndOffset = endOffset;
        }
    }

    public class FileInfo
    {
        public string FileName;
        public long Offset;
        public long Size;
        public EAssetType AssetType;

        public FileInfo(string fileName, EAssetType assetType, long offset, long size)
        {
            this.FileName = fileName;
            this.AssetType = assetType;
            this.Offset = offset;
            this.Size = size;
        }
    }

    public enum EAssetType
    {
        Unknown = 0xB0,
        TextFile,
        BinaryFile,
        SceneLayout,
        GameScript,
        Canvas
    }

    public class GameAsset
    {

    }

    public class GameArchive : IDisposable
    {
        private readonly char[] ArchiveMagic = new char[6] { (char)71, (char)88, (char)97, (char)14, (char)07, (char)01 }; //GXa??? [ASCII]
        private readonly char[] HeaderCode = new char[6] { (char)08, (char)05, (char)01, (char)04, (char)05, (char)18 }; //HEADER [A1Z26]
        private readonly char[] NoFileCode = new char[6] { (char)14, (char)15, (char)04, (char)01, (char)20, (char)01 }; //NODATA [A1Z26]
        private readonly char[] HeaderEnd = new char[4] { (char)08, (char)05, (char)14, (char)04 }; //HEND [A1Z26]
        private readonly char[] FileEnd = new char[2] { (char)0xED, (char)0xFE }; //237, 254 [DECIMAL]

        public HeaderInfo ArchiveHeaderInfo;
        public Dictionary<string, FileInfo> ArchiveFileMap;

        private StreamWriter ArchiveStream;

        public string ArchivePath { get; set; }

        public GameArchive(string ArchivePath)
        {
            this.ArchivePath = ArchivePath;
            if (File.Exists(ArchivePath))
            {
                this.Open();
            }
            else
            {
                this.New();
            }
        }

        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    ArchiveStream.Close();
                    ArchiveStream.Dispose();
                }

                disposedValue = true;
                Debug.WriteLine("Disposed GameArchive" + Path.GetFileName(ArchivePath));
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }

        private void BuildHeaderInfo()
        {
            MainWindow window = (MainWindow)Application.Current.MainWindow;

            long endingBytePosition = 0;
            ArchiveHeaderInfo.BeginOffset = 27;
            ArchiveStream.BaseStream.Position = 27;

            endingBytePosition = ArchiveStream.BaseStream.SearchBytes(ArchiveStream.BaseStream.Position, Encoding.Default.GetBytes(HeaderEnd));
            if (endingBytePosition >= 0)
            {
                ArchiveHeaderInfo.EndOffset = endingBytePosition;
                window.UpdateStatus("SUCCESS : Header ending bytes found at position " + endingBytePosition.ToString());
            }
            else
            {
                window.UpdateStatus("ERROR : Header ending bytes not found... Is GameArchive corrupted ?");
            }
        }

        private void BuildFileMap()
        {
            byte[] FileNameBuffer;
            byte[] FileSizeBuf;
            byte[] FileOffsetBuf;
            int CurrentByte, AssetByte;
            long FileSize, FileOffset;
            long EndFilePosition, EndFileName, EndOffsetPos;

            ArchiveFileMap = new Dictionary<string, FileInfo>();

            ArchiveStream.BaseStream.Position = ArchiveHeaderInfo.BeginOffset;
            while (ArchiveStream.BaseStream.Position < ArchiveHeaderInfo.EndOffset)
            {
                CurrentByte = ArchiveStream.BaseStream.ReadByte();
                if (CurrentByte >= 0xB0 && CurrentByte <= 0xBF)
                {
                    AssetByte = CurrentByte;
                    EndFilePosition = ArchiveStream.BaseStream.SearchBytes(ArchiveStream.BaseStream.Position, Encoding.Default.GetBytes(FileEnd));
                    EndFileName = ArchiveStream.BaseStream.SearchBytes(ArchiveStream.BaseStream.Position, Encoding.Default.GetBytes(new char[]{ (char)0xA0 }));
                    EndOffsetPos = ArchiveStream.BaseStream.SearchBytes(ArchiveStream.BaseStream.Position, Encoding.Default.GetBytes(new char[]{ (char)0xA1 }));

                    FileNameBuffer = new byte[EndFileName - ArchiveStream.BaseStream.Position];
                    ArchiveStream.BaseStream.Read(FileNameBuffer, 0, Convert.ToInt32(EndFileName - ArchiveStream.BaseStream.Position));
                    
                    ArchiveStream.BaseStream.Position = EndFileName + 1;
                    FileOffsetBuf = new byte[8];
                    ArchiveStream.BaseStream.Read(FileOffsetBuf, 0, 8);
                    FileOffset = BitConverter.ToInt64(FileOffsetBuf, 0);
                    
                    ArchiveStream.BaseStream.Position = EndOffsetPos + 1;
                    FileSizeBuf = new byte[8];
                    ArchiveStream.BaseStream.Read(FileSizeBuf, 0, 8);
                    FileSize = BitConverter.ToInt64(FileSizeBuf, 0);
                    
                    ArchiveFileMap[Encoding.ASCII.GetString(FileNameBuffer)] = new FileInfo(Encoding.ASCII.GetString(FileNameBuffer), (EAssetType)Enum.Parse(typeof(EAssetType), AssetByte.ToString()), FileOffset, FileSize);
                }
            }

            if (ArchiveFileMap.Count == 0)
            {
                Debug.WriteLine("No file found in GameArchive");
            }
        }

        private void New()
        {
            MainWindow window = (MainWindow)Application.Current.MainWindow;

            ArchiveStream = new StreamWriter(File.Open(ArchivePath, FileMode.Create))
            {
                AutoFlush = false
            };
            ArchiveStream.BaseStream.Position = 0;
            ArchiveStream.Write(ArchiveMagic);
            ArchiveStream.Write("VFS_GAMEARCHIVE");
            ArchiveStream.Write(HeaderCode);
            ArchiveStream.Write(NoFileCode);
            ArchiveStream.Write(HeaderEnd);

            this.BuildHeaderInfo();
            this.BuildFileMap();

            window.UpdateStatus("GameArchive " + Path.GetFileName(ArchivePath) + " created !");
        }

        private void Open()
        {
            MainWindow window = (MainWindow)Application.Current.MainWindow;

            ArchiveStream = new StreamWriter(File.Open(ArchivePath, FileMode.Open));

            this.BuildHeaderInfo();
            this.BuildFileMap();

            window.UpdateStatus("GameArchive " + Path.GetFileName(ArchivePath) + " opened !");
        }
    }
}
