﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace GameArchive_Manager
{
    public static class BuiltInExtension
    {
        private static int PadLeftSequence(byte[] bytes, byte[] seqBytes)
        {
            int i = 1;
            while (i < bytes.Length)
            {
                int n = bytes.Length - i;
                byte[] aux1 = new byte[n];
                byte[] aux2 = new byte[n];
                Array.Copy(bytes, i, aux1, 0, n);
                Array.Copy(seqBytes, aux2, n);
                if (aux1.SequenceEqual(aux2))
                    return i;
                i++;
            }
            return i;
        }

        public static long SearchBytes(this Stream stream, long StartPosition, byte[] byteSequence)
        {
            stream.Position = StartPosition;
            if (byteSequence.Length > stream.Length)
            {
                Debug.WriteLine("ByteSequence Length is greater than Stream Length");
                stream.Position = StartPosition;
                return -1;
            }

            byte[] buffer = new byte[byteSequence.Length];

            int i;
            while ((i = stream.Read(buffer, 0, byteSequence.Length)) == byteSequence.Length)
            {
                if (byteSequence.SequenceEqual(buffer))
                {
                    long Result = stream.Position - byteSequence.Length;
                    stream.Position = StartPosition;
                    return Result;
                }
                else
                {
                    stream.Position -= byteSequence.Length - PadLeftSequence(buffer, byteSequence);
                }
            }

            Debug.WriteLine("ByteSequence not found...");
            stream.Position = StartPosition;
            return -1;
        }
    }
}
