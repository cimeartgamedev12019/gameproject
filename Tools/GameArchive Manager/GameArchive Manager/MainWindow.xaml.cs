﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace GameArchive_Manager
{
    public partial class MainWindow : Window
    {
        public HashSet<string> FolderList;
        public bool IsArchiveOpen { get; set; }
        public GameArchive gameArchive = null;
        public MainWindow()
        {
            InitializeComponent();
            archiveTreeView.Items.Clear();
        }

        public void UpdateStatus(string StatusMessage)
        {
            statusLabel.Content = StatusMessage;
            Debug.WriteLine(StatusMessage);
        }

        public TreeViewItem FindItemByHeader(ItemsControl view, string header, out ItemsControl previousItemList)
        {
            foreach (TreeViewItem item in view.Items)
            {
                if ((string)item.Header == header)
                {
                    previousItemList = item;
                    return item;
                }
            }

            previousItemList = view;
            return null;
        }

        public void UpdateFileTree()
        {
            archiveTreeView.Items.Clear();

            if (gameArchive.ArchiveFileMap.Count == 0)
            {
                lblGuide.Visibility = Visibility.Visible;
                lblGuide.Content = "This archive is empty";
            }
            else
            {
                lblGuide.Content = "Create or Open a Game Archive to see it's content";
                lblGuide.Visibility = Visibility.Hidden;

                foreach(FileInfo file in gameArchive.ArchiveFileMap.Values)
                {
                    int lastPathDelimiterIdx = file.FileName.LastIndexOf('/');
                    if (lastPathDelimiterIdx != -1)
                    {
                        List<string> Directories = new List<string>(file.FileName.Substring(0, lastPathDelimiterIdx).Split('/'));
                        ItemsControl lastItem = archiveTreeView;
                        ItemsControl prevList = archiveTreeView;
                        foreach (string Directory in Directories)
                        {
                            lastItem = FindItemByHeader(lastItem, Directory, out prevList);
                            if (lastItem == null)
                            {
                                TreeViewItem newDirItem = new TreeViewItem
                                {
                                    Header = Directory
                                };
                                prevList.Items.Add(newDirItem);
                                lastItem = newDirItem;
                            }
                        }

                        TreeViewItem fileItem = new TreeViewItem
                        {
                            Header = file.FileName.Substring(lastPathDelimiterIdx + 1),
                            ToolTip = "Size : " + file.Size + " bytes",
                            Tag = file.FileName
                        };

                        fileItem.Selected += this.FileItem_Selected;

                        lastItem.Items.Add(fileItem);
                    }
                }
            }

            UpdateStatus("Archive opened successfully !");
        }

        private void FileItem_Selected(object sender, RoutedEventArgs e)
        {
            TreeViewItem item = (TreeViewItem)sender;
            FileInfo fileInfo = gameArchive.ArchiveFileMap[item.Tag.ToString()];
            UpdateStatus("INFO : " + fileInfo.FileName + " is a " + fileInfo.AssetType);
        }

        private void SaveFileDialog_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            SaveFileDialog saveFileDialog = (SaveFileDialog)sender;

            if (!File.Exists(saveFileDialog.FileName))
            {
                gameArchive = new GameArchive(saveFileDialog.FileName);
                IsArchiveOpen = true;
                UpdateFileTree();
            }
            else
            {
                MessageBox.Show("You are not allowed to overwrite a Game Archive! Use the 'Open' command or delete the Game Archive first...", "Access denied !", MessageBoxButton.OK, MessageBoxImage.Error);
                UpdateStatus("Archive opening operation cancelled...");
            }
        }

        private void OpenFileDialog_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            IsArchiveOpen = true;
            OpenFileDialog openFileDialog = (OpenFileDialog)sender;
            gameArchive = new GameArchive(openFileDialog.FileName);
            UpdateFileTree();
        }

        private void New_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog
            {
                Title = "Select where to save your Game Archive",
                Filter = "Game Archive (*.gxa)|*.gxa|All files (*.*)|*.*",
                InitialDirectory = Environment.CurrentDirectory,
                OverwritePrompt = false,
            };

            saveFileDialog.FileOk += this.SaveFileDialog_FileOk;
            saveFileDialog.ShowDialog();
        }

        private void Open_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                Title = "Select a Game Archive",
                Filter = "Game Archive (*.gxa)|*.gxa|All files (*.*)|*.*",
                InitialDirectory = Environment.CurrentDirectory
            };

            openFileDialog.FileOk += this.OpenFileDialog_FileOk;
            openFileDialog.ShowDialog();
        }

        private void Close_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            IsArchiveOpen = false;
            archiveTreeView.Items.Clear();
            UpdateStatus("Closed GameArchive " + Path.GetFileName(gameArchive.ArchivePath) + "...");
            gameArchive.Dispose();
            gameArchive = null;

            lblGuide.Content = "Create or Open a Game Archive to see it's content";
            lblGuide.Visibility = Visibility.Visible;
        }

        private void Save_Executed(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void SaveAs_Executed(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void CanExecuteHandler(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = IsArchiveOpen;
        }
        private void CanNotExecuteHandler(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = !IsArchiveOpen;
        }
    }
}
