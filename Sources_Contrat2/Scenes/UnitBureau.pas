Unit UnitBureau;

Interface

Uses SceneSystem, crt;

type
    SceneBureau = class(Scene)
        procedure Interaction_Note(choiceName : string);
        procedure Interaction_Echantillon_De_Sang(choiceName : string);
        procedure Interaction_Echantillon_De_Sang_Inconnu(choiceName : string);
        procedure Interaction_Rapport_Medecin_Legiste(choiceName : string);
        procedure Interaction_Couteau(choiceName : string);
        procedure Interaction_Livre(choiceName : string);
        procedure Interaction_Collegue(choiceName : string);
        procedure Interaction_Sortir(choiceName : string);
        procedure OnChoice(choiceName : string); override;
    end;

var
    Bureau : SceneBureau;
    x : integer;
    SortieReached, HasBook, HasNote, TalkColleague : boolean;

implementation
    procedure SceneBureau.Interaction_Sortir(choiceName : string);
    begin
        case choiceName of
			'Oui' :
			begin
				WriteText('*Vous quittez le du bureau*');
				SortieReached := true;
			end;
			
			'Non' : 
			begin
				WriteText('');
			end;
		end;
    end;

    procedure SceneBureau.Interaction_Note(choiceName : string);
    begin
        case choiceName of
            'Examiner' :
            begin
                WriteText (''#171'On peut lire sur cette note : "Dans la caverne de la for'#234't du Bois Sauvage '#224' 23h00. Prend le Livre avec toi." Humm ... On dirait un lieu de rendez'#45'vous.'#187''); 
            end;
            
            'Prendre' :
            begin
                Textcolor (lightgray);
                WriteText ('*Vous mettez la Note dans votre poche*');
                Textcolor (white);
                HasNote := true;
            end;

            'Retour':
            begin
                SceneBureau.Onechoice;
            end;
        end;
    end;

    procedure SceneBureau.Interaction_Echantillon_De_Sang(choiceName : string);
    begin 
       case choiceName of
			'Examiner' :
			begin
				WriteText (''#171'Le sang de la victime.'#187'');
			end;
			
			'Retour' : 
			begin
				
			end;
		end;
        

    end;

    procedure SceneBureau.Interaction_Echantillon_De_Sang_Inconnu(choiceName : string);
    begin
        case choiceName of
			'Examiner' :
			begin
				WriteText (''#171'Un '#233'chantillon de sang inconnu retrouv'#233' sur la sc'#232'ne de crime.'#187'');
			end;
			
			'Retour' : 
			begin
				
			end;
		end;
    end;

    procedure SceneBureau.Interaction_Rapport_Medecin_Legiste(choiceName : string);
    begin
        case choiceName of
			'Examiner' :
			begin
				WriteText (''#171'Il est '#233'crit sur le rapport : "James Starent. 25 ans. Mort par 2 coup de couteau dans l''abdomen et un coup port'#233' '#224' la nuque. La mort date de 26h."'#187'');
			end;
			
			'Retour' : 
			begin
				
			end;
		end;
    end;

    procedure SceneBureau.Interaction_Couteau(choiceName : string);
    begin
        case choiceName of
			'Examiner' :
			begin
				WriteText (''#171'L''arme du crime. Un couteau de boucher classique.'#187'');
			end;
			
			'Retour' : 
			begin
				
			end;
		end;
    end;

    procedure SceneBureau.Interaction_Livre(choiceName : string);
    begin
        case choiceName of
            'Examiner' :
            begin
                WriteText (''#171''#199'a doit '#234'tre le livre mention'#233' par la Note. Il est orn'#233' d''un symbole occulte. Il est '#233'crit "Oph'#233'a" sur la page principale.'#187'');
            end;

            'Prendre' :
            begin
                Textcolor (lightgray);
                WriteText ('*Vous mettez le livre dans votre poche*');
                Textcolor (white);
                HasBook := true;
            end;
            
            'Retour':
            begin
              
            end;
        end;
    end;

    procedure SceneBureau.Interaction_Collegue(choiceName : string);
    begin
        case choiceName of
			'Discuter' :
			begin
				WriteText (''#171'Tu veux mon avis sur quel objets'#32'?'#187' : ');
                case choiceName of
                    'Note' :
                    begin
                        case choiceName of
                            'Avis':
                            begin
                               WriteText (''#171'Elle a '#233'tait '#233'crite au stylo bille, rapidement visiblement. Une caverne'#32'? Bizzare comme lui de rendez'#45'vous, surtout aussi tard. "Prend le livre'#32'?" Peut'#45''#234'tre pour le rendre '#224'quelqu''un. C''est une hipothèse.'#187'');
                            end;   
                        
                            'Retour' :
                            begin
                              
                            end;
                        end;
                    end;

                    'Echantillon de sang' :
                    begin
                        WriteText (''#171'C''est le sang de la victime. Il est de groupe sanguin O'#45'. C’est tr'#232's rare.'#187'');

                        'Retour'
                    end;

                    'Echantillon de sang inconnu' :
                    begin
                        WriteText (''#171'Du sang inconnu de groupe A+. C''est du sang humain. Peut'#45''#234'tre celui du tueur.'#187'');

                        'Retour'
                    end;

                    'Rapport' :
                    begin
                        WriteText (''#171'J''ai vu la victime avec le l'#233'giste. Il avait des marques de coups sur lui. Il s''est d'#233'fendu, mais pas assez. Il '#233'tait jeune. Le tueur y est pas aller de mains mortes. Il savait ce qu''il faisait.'#187'');

                        'Retour'
                    end;

                    'Couteau' :
                    begin
                        WriteText (''#171'L''arme du crime. Un couteau de cuisine. Ouais, le classique. Par contre, il s''est servi du manche pour lui infliger le coup fatal '#224' la nuque. '#201'trange ... Normalement la lame est plus efficace que le manche, tout le monde sait ça. Ben pas visiblement. '#187'');

                        'Retour'
                    end;
                    
                    'Livre' :
                    begin
                        WriteText (''#171'C''est un livre bien myst'#233'rieux. Il est orn'#233' d''un symbole occulte. Peut'#45''#234'tre un livre d''incantation. Ou alors juste de la d'#233'co. "Oph'#233'a"'#32'? Le nom d''une divinit'#233' peut'#45''#234'tre'#32'? En tout cas c''est la premi'#232're chose qui me vient '#224' l''esprit en lisant ça'#187'');

                        'Retour'
                    end;
                end;
            end;
            
            'Retour' : 
			begin
				
			end;
        end;
        TalkColleague := true;
    end;

    procedure SceneBureau.OnChoice(choiceName : string);
    begin
        case (lowercase(choiceName)) of
            'note' :
            begin
                Bureau.ItemList := ItemArray.Create('Examiner', 'Prendre');
                Self.ChoiceProcedure := @Interaction_Note;
            end;

            'echantillon de sang' : 
            begin
                Bureau.ItemList := ItemArray.Create('Examiner');
                Self.ChoiceProcedure := @Interaction_Echantillon_De_Sang;
            end;

            'echantillon de sang inconnu' :
            begin
                Bureau.ItemList := ItemArray.Create('Examiner');
                Self.ChoiceProcedure := @Interaction_Echantillon_De_Sang_Inconnu;
            end;

            'rapport' :
            begin
                Bureau.ItemList := ItemArray.Create('Examiner');
                Self.ChoiceProcedure := @Interaction_Rapport_Medecin_Legiste;					
            end;

            'couteau' :
            begin
                Bureau.ItemList := ItemArray.Create('Examiner');
                Self.ChoiceProcedure := @Interaction_Couteau;				
            end;

            'livre' :
            begin
                Bureau.ItemList := ItemArray.Create('Examiner', 'Prendre');
                Self.ChoiceProcedure := @Interaction_Livre;	
            end;
                                
            'collegue' :
            begin
                Bureau.ItemList := ItemArray.Create('Discuter');
                Self.ChoiceProcedure := @Interaction_Collegue;	
            end;
        end;
    end;
    
initialization
    Bureau := SceneBureau.Create ();
    Bureau.ItemList := ItemArray.Create ('note','echantillon de sang','echantillon de sang inconnu','rapport','couteau','livre','collegue');
    Bureau.Presentation := '        Apr'#232's '#234'tre aller sur la sc'#232'ne du crime, vous retournez '#224' votre bureau pour examiner les objets que vous avez trouv'#233'. Sur une table, vous disposez'#13#10'une note, un '#233'chantillon de sang de la victime, un '#233'cheantillon de sang inconnu, le rapport du m'#233'decin l'#233'giste, un couteau et un livre. Vous '#234'tes en'#13#10'compagnie de votre coll'#232'gue scientifique qui peux vous donner son avis sur les diff'#233'rents objets.'#13#10'';
    Bureau.BackgroundPath := 'Scenes/Images/Bureau_Rempli.png';

    if HasNote and HasBook and TalkColleague then
    begin
        Bureau.ItemList := ItemArray.Create('sortir');
	    Bureau.Presentation :='';
    end;
End.