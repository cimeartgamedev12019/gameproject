unit SceneSelector;

interface

uses SysUtils, ctypes, Math, SceneSystem, CSFMLGraphics, CSFMLWindow, CSFMLSystem, CSFMLConfig;

type
    SceneEntry = class
    private
        Selected : Boolean;
        IsLocked : Boolean;
        SceneInstance : Scene;
        Title : String;
        SceneLabel : PsfText;
        Font : PsfFont;
        SelectionRectangle : PsfRectangleShape;
    public
        constructor Create(Instance : Scene; SceneTitle : String; Locked : Boolean = false);
        procedure Draw(Idx : Integer);
    end;

    SceneMenu = class
    private
        Mode : sfVideoMode;
        Title : String;
        SceneBackground : PsfTexture;
        BackgroundSize : sfVector2f;
        SpriteBackground : PsfSprite;
        FontRegular : PsfFont;
        FontSemibold : PsfFont;
        Event : sfEvent;
        SceneArray : array of SceneEntry;
        CurrentScene : Scene;

    public
        Window : PsfRenderWindow;
        PanelSize, PanelPos : sfVector2f;
        BackgroundPanel : PsfRectangleShape;
        PanelTitle : PsfText;
        PanelSubTitle : PsfText;
        SelectedIdx : Integer;
        DrawMenu : Boolean;

    private
        procedure UpdateWindow();

    public
        procedure InitWindow();
        procedure AddScene(Entry : SceneEntry);

    end;

var Menu : SceneMenu;

function CreateVector2D(X, Y : cfloat) : sfVector2f;

implementation

    function CreateVector2D(X, Y : cfloat) : sfVector2f;
    begin
        Result.x := X;
        Result.y := Y;
    end;

    function CreateFloatRect(Left, Top, Width, Height : cfloat) : sfFloatRect;
    begin
        Result.left := Left;
        Result.top := Top;
        Result.width := Width;
        Result.height := Height;
    end;

    constructor SceneEntry.Create(Instance : Scene; SceneTitle : String; Locked : Boolean = false);
    begin
        Self.SceneInstance := Instance;
        Self.Title := SceneTitle;
        Self.IsLocked := Locked;

        Self.Font := sfFont_createFromFile('Fonts/Regular.otf');

        Self.SceneLabel := sfText_create();
        sfText_setFont(Self.SceneLabel, Self.Font);
        sfText_setString(Self.SceneLabel, PChar(Self.Title));
        sfText_setCharacterSize(Self.SceneLabel, 16);

        Self.SelectionRectangle := sfRectangleShape_create();
        sfRectangleShape_setFillColor(Self.SelectionRectangle, sfColor_fromRGBA(255, 255, 255, 190));
    end;

    procedure SceneEntry.Draw(Idx : Integer);
    begin
        Self.Selected := (Idx = Menu.SelectedIdx);
        if (Self.Selected) then
        begin
            sfRectangleShape_setPosition(Self.SelectionRectangle, CreateVector2D(Menu.PanelPos.x - (Menu.PanelSize.x * 0.5), Menu.PanelPos.y - (Menu.PanelSize.y * 0.5) + 82 + (24 * Idx)));
            sfRectangleShape_setSize(Self.SelectionRectangle, CreateVector2D(Menu.PanelSize.x, 24));
            sfRenderWindow_drawRectangleShape(Menu.Window, Self.SelectionRectangle, Nil);
            sfText_setColor(Self.SceneLabel, sfBlack);
        end else begin
            if Self.IsLocked then
            begin
                sfText_setColor(Self.SceneLabel, sfColor_fromRGBA(255, 255, 255, 100));
                sfText_setString(Self.SceneLabel, PChar('?????'));
            end else begin
                sfText_setString(Self.SceneLabel, PChar(Self.Title));
                sfText_setColor(Self.SceneLabel, sfWhite);
            end;
        end;

        sfText_setPosition(Self.SceneLabel, CreateVector2D(Menu.PanelPos.x - (Menu.PanelSize.x * 0.5) + 16, Menu.PanelPos.y - (Menu.PanelSize.y * 0.5) + 82 + (24 * Idx)));
        sfRenderWindow_drawText(Menu.Window, Self.SceneLabel, Nil);
    end;

    procedure SceneMenu.InitWindow();
    begin
        Mode.width :=  1024;
        Mode.height := 576;
        Mode.bitsPerPixel := 32;
        SelectedIdx := 0;
        Title := 'Choisissez une sc'#232'ne';
        Window := sfRenderWindow_create(Mode, PChar(Title), sfUint32(sfResize) or sfUint32(sfClose), Nil);
        if not Assigned(Window) then
            raise Exception.Create('Window initialization failed !');
        
        SceneBackground := sfTexture_createFromFile('Scenes/Images/TestBackground.jpg', Nil);
        SpriteBackground := sfSprite_create();
        sfSprite_setTexture(SpriteBackground, SceneBackground, sfTrue);

        BackgroundSize.x := Mode.width;
        BackgroundSize.y := Mode.height;
        
        BackgroundPanel := sfRectangleShape_create();
        sfRectangleShape_setFillColor(BackgroundPanel, sfColor_fromRGBA(0,0,0,200));

        FontRegular := sfFont_createFromFile('Fonts/Regular.otf');
        FontSemibold := sfFont_createFromFile('Fonts/Semibold.otf');

        PanelTitle := sfText_create();
        sfText_setFont(PanelTitle, FontRegular);
        sfText_setCharacterSize(PanelTitle, 16);
        sfText_setString(PanelTitle, 'Projet contrat 2');

        PanelSubTitle := sfText_create();
        sfText_setFont(PanelSubTitle, FontRegular);
        sfText_setCharacterSize(PanelSubTitle, 28);
        sfText_setString(PanelSubTitle, PChar(Title));
        
        DrawMenu := true;

        UpdateWindow();
    end;

    procedure SceneMenu.AddScene(Entry : SceneEntry);
    begin
        SetLength(Self.SceneArray, Length(SceneArray) + 1);
        Self.SceneArray[Length(SceneArray) - 1] := Entry;
    end;

    procedure SceneMenu.UpdateWindow();
    var
        Idx : Integer;
    begin
        while sfRenderWindow_IsOpen(Window) = sfTrue do
        begin
            while sfRenderWindow_pollEvent(Window, @Event) = sfTrue do
            begin
                if (Event.type_ = sfEvtClosed) then
                begin
                    sfRenderWindow_close(Window);
                end;

                if (Event.type_ = sfEvtResized) then
                begin
                    sfRenderWindow_setView(Window, sfView_createFromRect(CreateFloatRect(0, 0, Event.size.width, Event.size.height)));
                end;

                if (Event.type_ = sfEvtKeyPressed) then
                begin
                    if (Event.key.code = sfKeyDown) then
                    begin
                        if DrawMenu then
                        begin
                            SelectedIdx := Max(0, Min(SelectedIdx + 1, Length(SceneArray) - 1));
                            while SceneArray[SelectedIdx].IsLocked do
                            begin
                                SelectedIdx := Max(0, Min(SelectedIdx + 1, Length(SceneArray) - 1));
                            end;
                        end else begin
                            CurrentScene.ChIdx := Max(0, Min(CurrentScene.ChIdx + 1, Length(CurrentScene.ItemList) - 1));
                        end;
                    end;

                    if (Event.key.code = sfKeyUp) then
                    begin
                        if DrawMenu then
                        begin
                            SelectedIdx := Max(0, Min(SelectedIdx - 1, Length(SceneArray) - 1));
                            while SceneArray[SelectedIdx].IsLocked do
                            begin
                                SelectedIdx := Max(0, Min(SelectedIdx - 1, Length(SceneArray) - 1));
                            end;
                        end else begin
                            CurrentScene.ChIdx := Max(0, Min(CurrentScene.ChIdx - 1, Length(CurrentScene.ItemList) - 1));
                        end;
                    end;

                    if (Event.key.code = sfKeyU) then
                    begin
                        for Idx := 0 to Length(SceneArray) - 1 do
                        begin
                            SceneArray[Idx].IsLocked := False;
                        end;
                    end;

                    if (Event.key.code = sfKeyEnter) then
                    begin
                        if DrawMenu then
                        begin
                            DrawMenu := false;
                            CurrentScene := SceneArray[SelectedIdx].SceneInstance;
                            CurrentScene.WindowReference := Menu.Window;
                            CurrentScene.Display();
                        end else begin
                            WriteLn('SELECTED CHOICE : ', CurrentScene.ItemList[CurrentScene.ChIdx]);
                            CurrentScene.ChoiceProcedure(CurrentScene.ItemList[CurrentScene.ChIdx]);
                            CurrentScene.ChIdx := 0;
                        end;
                    end;
                end;
            end;
            
            BackgroundSize.x := sfRenderWindow_getSize(Window).x / sfTexture_getSize(sfSprite_getTexture(SpriteBackground)).x;
            BackgroundSize.y := sfRenderWindow_getSize(Window).y / sfTexture_getSize(sfSprite_getTexture(SpriteBackground)).y;

            if DrawMenu then
            begin
                sfRenderWindow_clear(Window, sfBlack);

                sfSprite_setScale(SpriteBackground, BackgroundSize);
                sfRenderWindow_drawSprite(Window, SpriteBackground, nil);

                PanelSize.x := Mode.width - (0.25 * Mode.width);
                PanelSize.y := Mode.height - (0.25 * Mode.height);
                PanelPos := CreateVector2D(sfView_getSize(sfRenderWindow_getView(Window)).x * 0.5, sfView_getSize(sfRenderWindow_getView(Window)).y * 0.5);

                sfRectangleShape_setOrigin(BackgroundPanel, CreateVector2D(PanelSize.x * 0.5, PanelSize.y * 0.5));
                sfRectangleShape_setPosition(BackgroundPanel, PanelPos);
                sfRectangleShape_setSize(BackgroundPanel, PanelSize);
                sfRenderWindow_drawRectangleShape(Window, BackgroundPanel, nil);

                sfText_setPosition(PanelTitle, CreateVector2D(PanelPos.x - (PanelSize.x * 0.5) + 16, PanelPos.y - (PanelSize.y * 0.5) + 8));
                sfRenderWindow_drawText(Window, PanelTitle, nil);

                sfText_setPosition(PanelSubTitle, CreateVector2D(PanelPos.x - (PanelSize.x * 0.5) + 16, PanelPos.y - (PanelSize.y * 0.5) + sfText_getCharacterSize(PanelTitle) + 16));
                sfRenderWindow_drawText(Window, PanelSubTitle, nil);
                
                for Idx := 0 to Length(SceneArray) - 1 do
                begin
                    if not (SceneArray[Idx] = Nil) then
                    begin
                        SceneArray[Idx].Draw(Idx);
                    end;
                end;
            end else begin
                CurrentScene.Draw();
            end;

            sfRenderWindow_display(Window);
        end;

        sfRectangleShape_destroy(BackgroundPanel);
        sfRenderWindow_destroy(Window);
    end;

initialization
    Menu := SceneMenu.Create();

end.