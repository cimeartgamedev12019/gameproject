unit UnitTest;

interface

uses SceneSystem;

type
    SceneTest = class(Scene)
        procedure OnChoice(choiceName : string); override;
    end;

var
    Test : SceneTest;

implementation
    procedure SceneTest.OnChoice(choiceName : String);
    begin
        case (LowerCase(choiceName)) of
            'un livre':
            begin
                WriteLn('Vous ouvrez le livre');
            end;
        end;
    end;

initialization
    Test := SceneTest.Create();
    Test.Presentation := 'Bienvenue dans la scene de test elle contient $un livre$ pos'#233' sur une table.'#13#10'A gauche de la piece il y a un meuble avec $une lettre$ dessus';

end.