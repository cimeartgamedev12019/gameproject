unit UnitTest2;

interface

uses SceneSystem;

type
    SceneTest2 = class(Scene)
        procedure OnChoice(choiceName : String); override;
    end;

var
    Test2 : SceneTest2;
    HasKey : Boolean;

implementation

procedure SceneTest2.OnChoice(choiceName : String);
begin
    case (LowerCase(choiceName)) of
        'ballon' :
        begin
            WriteLn('Vous rangez le ballon dans l''armoire');
            WriteLn('Vous trouvez une cl'#233' dans l''armoire !');
            HasKey := true;
        end;

        'poubelle' :
        begin
            WriteLn('Vous videz la poubelle');
        end;
    end;
end;

initialization
    Test2 := SceneTest2.Create();
    Test2.Presentation := 'Vous entrez dans la chambre de toto il y a un $ballon$ sur le bureau. La $poubelle$ est pleine.';

end.