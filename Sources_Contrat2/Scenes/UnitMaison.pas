UNIT UnitMaison;
interface
uses SceneSystem, crt;

type
	SceneScene1 = class(Scene)
	procedure InteractionFiche(choiceName: String);
	procedure InteractionTelephone(choiceName : String);
	procedure InteractionSortir(choiceName:string);
	procedure OnChoice(choiceName:string); override;
end;

var
	LivreLi:boolean = false;
	FinScene:boolean = false;
	SiSortir:boolean = false;
	Scene1:SceneScene1;
	
implementation

procedure SceneScene1.InteractionFiche(choiceName:string);
begin
	case choiceName of
		'Examiner':
		begin
			WriteText('C''est une fiche qui donne des informations sur le monde et comment il fonctionne');
		end;
		'Lire':
		begin
			WriteText('Au file de votre partie vous allez découvrir des objet avec lesquelles vous pourrez interagir chaque objet est different et plusieurs type d''interaction sont a noter : ');
			WriteText('Examinez ,Lire ,Utilisez et bien d''autre que vous découvrirez au file de votre aventure');
			LivreLi := true;
		end;
	end;
end;

procedure SceneScene1.InteractionTelephone(choiceName:string);
begin
	case choiceName of
		'Examiner':
		begin
			WriteText('Ce téléphone est a vous il y a un message non lue');
			Scene1.OnChoice(choiceName);
		end;
		'Utiliser':
		begin
			WriteText('Vous lisez le message non lue ,il est écrit que vous devez vous rendre sur une scene de crime ');
			WriteText('dans une chambre ou une personne est morte de maniere étrange, aucune piste,aucun indice il ont besoin de vous : le meilleur detective');
			WriteText('Cette histoire c''est votre histoire ,votre enquête');
			SiSortir := true;
			Scene1.OnChoice(choiceName);
		end;
	end;
end;

procedure SceneScene1.InteractionSortir(choiceName:string);
begin
	case choiceName of
		'Oui' :
		begin
			WriteText('Vous Sortez');
			FinScene := true;
		end;
		
		'Non' : 
		begin
			WriteText('Vous rester a l''interieur.');
		end;
	end;
end;

procedure SceneScene1.OnChoice(choiceName :string);
begin
	while FinScene = false do
	begin
		case choiceName of
			'Fiche':
			begin
				Scene1.ItemList := ItemArray.Create('Examiner', 'Lire');
				Self.ChoiceProcedure := @InteractionFiche;
			end;
			'Telephone':
			begin
				if LivreLi = true then
				begin
					Scene1.ItemList := ItemArray.Create('Examiner', 'Utiliser');
					Self.ChoiceProcedure := @InteractionTelephone;
				end
				else
				begin
					WriteText('vous devriez peut être lire la fiche d''information en premier lieu');
				end;
			end;
			'Sortir':
			begin
				Scene1.ItemList := ItemArray.Create('Oui','Non');
				Self.ChoiceProcedure := @InteractionSortir;
			end;
		end;
	end;
end;

Initialization
	Scene1 := SceneScene1.Create();
	Scene1.Presentation := 'devant vous, vous voyez un ecran d''ordinateur et vous pouvez interagir avec les élément suivant :';
	Scene1.BackgroundPath := 'Scenes/Images/Images_contrat2_UnitScene1/Scene1.jpg';
	Scene1.ItemList := ItemArray.Create('Fiche');
	
	if SiSortir = true then
	begin
		Scene1.ItemList := ItemArray.Create('Fiche','Telephone','Sortir');
	end;

	if LivreLi = true then
	begin
		Scene1.Presentation := 'Vous remarquez un telephone sur votre droite';
		Scene1.ItemList := ItemArray.Create('Fiche','Telephone');
		Scene1.BackgroundPath := 'Scenes/Images/Images_contrat2_UnitScene1/Scene1AvecTelephone.JPG';
	end;
end.