unit UnitCaverne;

interface

uses SceneSystem;

type
    SceneCaverne = class(Scene)
        procedure OnChoice(nomchoix : string); override;
    end;

var
    Caverne : SceneCaverne;

implementation
    procedure SceneCaverne.OnChoice(nomchoix : string);
    begin        
        WriteText('Vous : "Il fait sombre...Mais la lumi'#232're provenant de l''ext'#233'rieur me permet de voir sur quelques m'#232'tre."');
        UpdateImage('Scenes/Images/Images_contrat2_UnitCaverne/CavernePhreatique.jpg');
        WriteText('Quand vous entrez, vous voyez d''abord un stalagmite et un stalactite. Puis, plus loin, un pentagramme rouge au sol apparait dans votre champ de vision. Que Voulez-vous examiner ?');
        case (LowerCase(nomchoix)) of
            'stalagmite':
            begin
                WriteText('Vous : "Cette caverne est tr'#232's vieille, on dirait une ancienne nappe phr'#233'atique..."');
            end;

            'stalactite':
            begin
                WriteText('Vous : "Je ne peux pas l''atteindre, le plafond de la caverne est trop haut..."');
            end;
            
            'pentagramme':
            begin
                UpdateImage('Scenes/Images/Images_contrat2_UnitCaverne/SolCaverne00.png');
                WriteText('Vous : "Il a '#233't'#233' dessin'#233' avec du sang, il est sec, ça s’est pass'#233' il y a un moment, tout comme le meurtre... Il y a un post-it au milieu."');
                WriteText('Vous prenez le post-it.');

                UpdateImage('Scenes/Images/Images_contrat2_UnitCaverne/SolCaverne01.png');
                WriteText('Il est '#233'crit : "Pose le livre au cente du pentagramme."');
                WriteText('Vous posez le livre au centre du pentagramme.');

                UpdateImage('Scenes/Images/Images_contrat2_UnitCaverne/SolCaverne02.png');
                WriteText('Vous : "Bizarre...Il s''est ouvert tout seul..!"');
                UpdateImage('Scenes/Images/Images_contrat2_UnitCaverne/SolCaverne03.png');
                WriteText('Un vortex se forme soudainement.');
                UpdateImage('Scenes/Images/Images_contrat2_UnitCaverne/SolCaverne04.png');
                WriteText('Vous vous faites aspirer dans un monde inconu et magique !');   
                UpdateImage('Scenes/Images/Images_contrat2_UnitCaverne/SolCaverne04.png');

                WriteText('Vous entendez une voix feminine avant de tomber inconcient.');
                UpdateImage('Scenes/Images/Images_contrat2_UnitCaverne/OeilOphea.jpg');
                WriteText('Voix : "Viens '#225' moi..."');
                WriteText('To Be Continued...');
            end;
        end;
    end;

initialization
    Caverne := SceneCaverne.Create();
    Caverne.BackgroundPath := 'Scenes/Images/Images_contrat2_UnitCaverne/CaverneGrande.jpg';
    Caverne.Presentation := 'Apr'#232's avoir lu le post-it, pris le livre et laiss'#233' votre ami interoger seul les voisins de la victime, vous vous rendez sur le lieu de rendez-vous pr'#233'sum'#233' : la caverne.';
    Caverne.ItemList := ItemArray.Create('pentagramme', 'stalactite', 'stalagmite');
end.