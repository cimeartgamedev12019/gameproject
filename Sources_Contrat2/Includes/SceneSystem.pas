unit SceneSystem;

interface

uses SysUtils, crt, ctypes, CSFMLWindow, CSFMLGraphics, CSFMLConfig, CSFMLSystem, DOM, XMLRead, XMLWrite, XMLCfg, XMLUtils, XMLStreaming;

type
    TChoiceProcedure = procedure(choiceName : String) of object;
    ItemArray = array of String;

    Scene = class
        Presentation : String;
        ItemList : array of String;
        WindowReference : PsfRenderWindow;
        BackgroundTexture : PsfTexture;
        BackgroundSprite : PsfSprite;
        BackgroundPath : String;
        BackgroundSize : sfVector2f;
        UserChoice : String;
        ChoiceString : String;
        ChIdx : Integer;
        WaitingChoice : Boolean;
        ChoiceProcedure : TChoiceProcedure;
        Event : sfEvent;

        DialogFont : PsfFont;
        DialogText : PsfText;
        DialogBox : PsfRectangleShape;
        DialogSize, DialogPosition : sfVector2f;

        procedure Display();
        procedure Draw();
        procedure WriteText(Message : String);
        procedure UpdateImage(ImagePath : String);
        procedure UpdateText(Message : String);
        procedure OnChoice(choiceName : String); virtual;
        procedure BeginScene(); virtual;
        procedure WaitChoice();
        procedure Wait(TickCount : Integer);
        constructor Create();

    end;

function CreateVector2D(X, Y : cfloat) : sfVector2f;
function CreateFloatRect(Left, Top, Width, Height : cfloat) : sfFloatRect;

implementation

    function CreateVector2D(X, Y : cfloat) : sfVector2f;
    begin
        Result.x := X;
        Result.y := Y;
    end;

    function CreateFloatRect(Left, Top, Width, Height : cfloat) : sfFloatRect;
    begin
        Result.left := Left;
        Result.top := Top;
        Result.width := Width;
        Result.height := Height;
    end;

    constructor Scene.Create();
    begin
        SetLength(ItemList, 0);
        Self.ChoiceProcedure := @Self.OnChoice;
    end;

    procedure Scene.OnChoice(choiceName : String);
    begin
        
    end;

    procedure Scene.Wait(TickCount : Integer);
    var tc : Integer;
    begin
        while (tc < TickCount) do
        begin
            WriteLn(tc);
            Inc(tc);
            Self.Draw();
            sfRenderWindow_display(WindowReference);
        end;
    end;

    procedure Scene.BeginScene();
    begin
        WriteLn('Begin scene...');
        Self.ChoiceProcedure := @Self.OnChoice;
    end;

    procedure Scene.WaitChoice();
    begin
        ChIdx := 0;
        WaitingChoice := true;
    end;

    procedure Scene.Display();
    begin
        System.WriteLn('Loading scene...');
        BackgroundTexture := sfTexture_createFromFile(PChar(BackgroundPath), Nil);
        BackgroundSprite := sfSprite_create();
        sfSprite_setTexture(BackgroundSprite, BackgroundTexture, sfTrue);

        DialogBox := sfRectangleShape_create();
        sfRectangleShape_setFillColor(DialogBox, sfColor_fromRGBA(0,0,0,200));

        DialogFont := sfFont_createFromFile('Fonts/Regular.otf');
        DialogText := sfText_create();

        sfText_setFont(DialogText, DialogFont);
        sfText_setCharacterSize(DialogText, 16);
        WriteText(Self.Presentation);
    end;

    procedure Scene.Draw();
    begin
        sfRenderWindow_clear(WindowReference, sfBlack);

        BackgroundSize.x := sfRenderWindow_getSize(WindowReference).x / sfTexture_getSize(sfSprite_getTexture(BackgroundSprite)).x;
        BackgroundSize.y := sfRenderWindow_getSize(WindowReference).y / sfTexture_getSize(sfSprite_getTexture(BackgroundSprite)).y;
        
        sfSprite_setScale(BackgroundSprite, BackgroundSize);
        sfRenderWindow_drawSprite(WindowReference, BackgroundSprite, Nil);

        DialogSize := CreateVector2D(sfView_getSize(sfRenderWindow_getView(WindowReference)).x, sfView_getSize(sfRenderWindow_getView(WindowReference)).y * 0.2);
        DialogPosition := CreateVector2D(0, sfView_getSize(sfRenderWindow_getView(WindowReference)).y * 0.8);

        sfRectangleShape_setSize(DialogBox, DialogSize);
        sfRectangleShape_setPosition(DialogBox, DialogPosition);

        sfRenderWindow_drawRectangleShape(WindowReference, DialogBox, Nil);

        sfText_setPosition(DialogText, DialogPosition);
        sfRenderWindow_drawText(WindowReference, DialogText, Nil);
        
        ChoiceString := sLineBreak + 'Selectionner votre choix : ' + '< ' + ItemList[ChIdx] + ' >';

        UpdateText(Self.Presentation);
    end;

    procedure Scene.WriteText(Message : String);
    begin
        Self.Presentation := Message;
    end;

    procedure Scene.UpdateText(Message : String);
    begin
        sfText_setString(DialogText, PChar(Message + ChoiceString));
    end;

    procedure Scene.UpdateImage(ImagePath : String);
    begin
        sfTexture_destroy(BackgroundTexture);
        BackgroundTexture := sfTexture_createFromFile(PChar(ImagePath), Nil);
        sfSprite_setTexture(BackgroundSprite, BackgroundTexture, sfTrue);
    end;
end.