Unit objets;

uses Personnage.pas,crt;

type
	item : object;
	nom :string;
	function utiliser(dialogue:string,use:boolean = false) : boolean;
	procedure examiner(dialogue:string);
	procedure prendre(dialogue:string,use:boolean = false);
	procedure discuter(dialogue:string,use:boolean = false);
	constructor create(Cnom:string);
end;

procedure Item.examiner(dialogue:string);
begin
	writeln(dialogue);
end;

procedure Item.prendre(dialogue:string,use:boolean = false);
begin
	writeln(dialogue);
	if use = true then
	begin
		while Personnage.InventoryItem[i].nom <> "" do
		begin
			Personnage.InventoryItem[i] := Item;
			Inc(i);
		end;
	end;	
end;

function Item.utiliser(dialogue:string, use:boolean = false) : boolean;
begin
	writeln(dialogue);
	utiliser := true;
end;

procedure Item.discuter(dialogue:string,use:boolean = false);
begin
	writeln(dialogue);
end;

